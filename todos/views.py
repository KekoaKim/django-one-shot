from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoListItemForm

# Create your views here.


def todo_list_list(request):
    todos = TodoList.objects.all

    context = {
        "todo_list": todos
    }

    return render(request, "todos_lists/list.html", context)


def todo_items(request, id):
    todos = get_object_or_404(TodoList, id=id)

    context = {
        "todo_object": todos,
    }

    return render(request, "todos_lists/detail.html", context)


def create_todo(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todos = form.save()
            return redirect("todo_list_detail", id=todos.id)
    else:
        form = TodoListForm()

    context = {
        "form": form
    }

    return render(request, "todos_lists/create.html", context)


def update_todo(request, id):
    todos = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todos)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=todos)

    context = {
        "form": form,
        "todo_object": todos,
    }

    return render(request, "todos_lists/update.html", context)


def delete_todo(request, id):
    todos = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todos.delete()
        return redirect("todo_list_list")

    return render(request, "todos_lists/delete.html")


def create_todo_item(request):
    if request.method == "POST":
        form = TodoListItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoListItemForm()

    context = {
        "form": form,
    }

    return render(request, "todos_lists/item_create.html", context)


def edit_todo_item(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoListItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todo_item.list.id)
    else:
        form = TodoListItemForm(instance=todo_item)

    context = {
        "form": form,
        "item_object": todo_item
    }

    return render(request, "todos_lists/item_edit.html", context)
