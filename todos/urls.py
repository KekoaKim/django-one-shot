from django.urls import path
from todos.views import todo_list_list, todo_items, create_todo, update_todo
from todos.views import delete_todo, create_todo_item, edit_todo_item

urlpatterns = [
    path("", todo_list_list, name="todo_list_list"),
    path("todos/<int:id>/", todo_items, name="todo_list_detail"),
    path("create/", create_todo, name="todo_list_create"),
    path("<int:id>/edit/", update_todo, name="todo_list_update"),
    path("<int:id>/delete/", delete_todo, name="todo_list_delete"),
    path("items/create/", create_todo_item, name="todo_item_create"),
    path("items/<int:id>/edit/", edit_todo_item, name="todo_item_update"),
]
